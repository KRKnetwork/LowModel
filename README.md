# LowModel

BaseModel for LowDB

## Installing

```shell
npm i @krknet/lowmodel
```

# Usage

```js
const BaseModel = require('@krknet/lowmodel')

// the name of the LowDB Collection
const dbrep = 'accounts'
// Object Schema
const schema = {
  id: null,
  mail: null,
  name: null,
  role: null,
  passphrase: null
}

module.exports = class Account extends BaseModel {

  static get dbrep () { return db.get(dbrep) }

  static get _modelSchema () { return schema }
}
```

## Developing

Issue List: [https://gitlab.com/KRKnetwork/lowmodel/issues](https://gitlab.com/KRKnetwork/lowmodel/issues)

[![js-standard-style](https://cdn.rawgit.com/standard/standard/master/badge.svg)](http://standardjs.com)
