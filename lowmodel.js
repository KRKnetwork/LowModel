const _ = require('lodash')
const deepmerge = require('deepmerge')

module.exports = class BaseModel {
  constructor (data = {}) {
    data.id = data.id || this._createID()

    this.data = this._sanitize(data)
  }

  update (data) {
    this.data = this._sanitize(data)

    return this
  }

  patch (data) {
    this.data = this._sanitize(deepmerge(this.data, data, { arrayMerge: (destination, source) => source }))

    return this
  }

  delete () {
    const collection = this.constructor.dbrep

    if (collection.isArray().value()) {
      collection.remove({ id: this.data.id }).write()
    } else {
      collection.unset(this.data.id).write()
    }

    return true
  }

  save () {
    this.data._lastChange = Date.now()

    const collection = this.constructor.dbrep

    if (collection.isArray().value()) {
      const index = collection.findIndex({ id: this.data.id }).value()

      if (index === -1) {
        collection.push(this.data).write()
      } else {
        collection.set(index, this.data).write()
      }
    } else {
      collection.set(this.data.id, this.data).write()
    }

    return this
  }

  static get (input) {
    let collection = this.dbrep

    switch (typeof input) {
      case 'undefined':
        // All Entries
        return collection.values().cloneDeep().value().map((d) => new this(d))
      case 'string':
        // By ID return Single Element or false
        let jResult = collection.find({ id: input }).cloneDeep().value()
        return jResult === undefined ? false : new this(jResult)
      case 'object':
        // By Filter
        return collection.filter(input).cloneDeep().value().map((d) => new this(d))
      default:
        // Unsupported Fallback
        return []
    }
  }

  static get dbrep () { return null }

  static get idLength () { return 5 }

  static get _baseSchema () { return { _created: Date.now(), _lastChange: Date.now() } }

  static get _modelSchema () { return {} }

  static get schema () { return _.defaultsDeep(this._modelSchema, this._baseSchema) }

  _sanitize (data = {}) {
    data._created = data._created || Date.now()

    return _.pick(_.defaultsDeep(data, this.constructor.schema), _.keys(this.constructor.schema))
  }

  toJSON () {
    return this.data
  }

  _createID () {
    const sID = ((a, b) => { for (b = a = ''; a++ < this.constructor.idLength; b += a * 51 & 52 ? (a ^ 15 ? 8 ^ Math.random() * (a ^ 20 ? 16 : 4) : 4).toString(16) : '-'); return b })()
    const jCheck = this.constructor.get(sID)

    return jCheck === false ? sID : this._createID()
  }
}
